Feature: Frontpage checks

  @critical
  Scenario: Frontpage is reachable with HTTP 200
    Given I am on the homepage
    Then the response status code should be 200
