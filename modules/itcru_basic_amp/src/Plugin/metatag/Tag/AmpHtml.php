<?php

namespace Drupal\itcru_basic_amp\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\LinkRelBase;

/**
 * Provides a plugin for the 'amphtml' meta tag.
 *
 * @MetatagTag(
 *   id = "amphtml",
 *   label = @Translation("Amp HTML"),
 *   description = @Translation("A link to the AMP page URL of the content of this page."),
 *   name = "amphtml",
 *   group = "advanced",
 *   weight = 2,
 *   type = "uri",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class AmpHtml extends LinkRelBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
