@critical
Feature: Node checks

  Scenario: Restrict node page as editor
    Given I am logged in as "test_editor" with "tEst123#"
    When I am on "/node/add/page"
    Then the response status code should be 403

  Scenario: Add node page as administrator
    Given I am logged in as "test_admin" with "tEst123#"
    When I am on "/node/add/page"
    Then the response status code should be 200

  Scenario: Add node article as editor
    Given I am logged in as "test_editor" with "tEst123#"
    When I am on "/node/add/article"
    Then the response status code should be 200

  Scenario: Add node event as editor
    Given I am logged in as "test_editor" with "tEst123#"
    When I am on "/node/add/event"
    Then the response status code should be 200

  @api @javascript
  Scenario: access editor form as editor
    Given I am logged in as "test_editor" with "tEst123#"
    And I am on "node/add/article"
    Then the response status code should be 200
    And I should see the "Speichern" button
