Feature: Sitemap checks

  @critical
  Scenario: Sitemap is accessible
    Given I am an anonymous user
    When I am on "sitemap.xml"
    Then the response status code should be 200
    And I should get a valid XML feed
