@critical
Feature: Security checks

  Scenario: Restrict access to media image form as anonymous user
    Given I am an anonymous user
    And I am on "media/add/image"
    Then the response status code should be 403

  Scenario: Restrict access to media video form as anonymous user
    Given I am an anonymous user
    And I am on "media/add/video"
    Then the response status code should be 403

  Scenario: Restrict access to node article form as anonymous user
    Given I am an anonymous user
    And I am on "node/add/article"
    Then the response status code should be 403

  Scenario: Restrict access to node event form as anonymous user
    Given I am an anonymous user
    And I am on "node/add/event"
    Then the response status code should be 403

  Scenario: Restrict access to node page form as anonymous user
    Given I am an anonymous user
    And I am on "node/add/page"
    Then the response status code should be 403

  Scenario: Restrict access to article overview as anonymous user
    Given I am an anonymous user
    And I am on "admin/content?status=All&type=All&title=&uid=&langcode=All"
    Then the response status code should be 403

  Scenario: Restrict access to term add form of channel vocabulary
    Given I am an anonymous user
    And I am on "admin/structure/taxonomy/manage/channel/add"
    Then the response status code should be 403

  Scenario: Restrict access to term add form of tags vocabulary
    Given I am an anonymous user
    And I am on "admin/structure/taxonomy/manage/tags/add"
    Then the response status code should be 403
